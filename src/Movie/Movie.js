import React from "react"
import {MovieProvider} from "./MovieContext"
import MovieForm from "./MovieForm"
import MovieList from "./MovieList"
import { getUser, removeUserSession } from '../Utils/Common';

const Movie = (props) => {
    const user = getUser();
    const handleLogout = () => {    
        removeUserSession();
        props.history.push('/login');
    }
    
    return(
        <section>
            <input type="button" onClick={handleLogout} value="Logout" />
            <br />
            <br />
            <MovieProvider>
                <MovieList />
                <br />
                <MovieForm />
            </MovieProvider>
        </section>
    )
}

export default Movie