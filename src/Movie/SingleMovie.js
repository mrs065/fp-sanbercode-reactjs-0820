import React, {useState, useEffect} from 'react'
import {useParams} from 'react-router-dom'
import axios from 'axios'
import { Breadcrumb, Layout, PageHeader } from 'antd';

const { Content } = Layout;

const SingleMovie = () => {
    let {id} = useParams()
    const [movie, setMovie] = useState(null)

    useEffect(() => {
        if(movie === null){
            axios.get(`https://backendexample.sanbersy.com/api/data-movie/${id}`)
            .then(res => {
                setMovie(res.data)
            })
        }
    }, [movie, setMovie])

    return (
        <Content style={{ margin: '0 16px' }}>
            <Breadcrumb style={{ margin: '16px 0' }}>
                <Breadcrumb.Item>Movies</Breadcrumb.Item>
                <Breadcrumb.Item>Movie Detail</Breadcrumb.Item>
            </Breadcrumb>
            <div className="site-layout-background" style={{ padding: 24, minHeight: 360 }}>
                
                <br />
                <div>
                    {movie !== null &&
                        <>
                            <PageHeader className="site-page-header" title={movie.title} />
                            <p>{movie.description}</p>
                            <p><img width='200' src={movie.image_url} /></p>
                        </>
                    }
                </div>
            </div>
        </Content>
    )
}

export default SingleMovie