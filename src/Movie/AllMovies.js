import React, {useState, useEffect, Component} from 'react'
import axios from 'axios'
import { Breadcrumb, Layout, PageHeader, Col, Row } from 'antd';
import { Link } from 'react-router-dom';

const { Content } = Layout;
const gutters = {};
const vgutters = {};

function minuteToHours(num){
    var hours = (num/60)
    var rhours = Math.floor(hours)
    var minutes = (hours - rhours)*60
    var rminutes = Math.round(minutes)
    return (rhours === 0 ? '' : rhours + ' Jam' + (rminutes===0 ? '' : ' ' + rminutes + ' Menit'))
}

class AllMovies extends Component {
    constructor(props){
        super(props)
        this.state = {
            dataFilm: [],
            gutterKey: 16,
            vgutterKey: 16,
            colCountKey: 4
        }
    }

    componentDidMount(){
        axios.get(`https://backendexample.sanbersy.com/api/data-movie`)
        .then(res => {
            let dataFilm = res.data.map(el => { return {
                id: el.id,
                title: el.title,
                rating: el.rating,
                duration: el.duration,
                genre: el.genre,
                description: el.description,
                image_url: el.image_url
            }})
            this.setState({dataFilm})
        })
    }

    render(){
        return(
            <Content style={{ margin: '0 16px' }}>
                <Breadcrumb style={{ margin: '16px 0' }}>
                    <Breadcrumb.Item>Movies</Breadcrumb.Item>
                </Breadcrumb>
                <div className="site-layout-background" style={{ padding: 24, minHeight: 360 }}>
                    <PageHeader className="site-page-header" title="Data Film Terbaik" />
                    <br />
                    <div>
                        <Row gutter={[gutters[this.state.gutterKey], vgutters[this.state.vgutterKey]]}>
                            {
                                this.state.dataFilm.map((item) => {
                                    return(
                                        <div>
                                            <Col span={6}>
                                                <div>
                                                    <img style={{width: "100%", height: "200px", objectFit: "cover", borderRadius: "4px"}} src={item.image_url} />
                                                    <br />
                                                    {/* <a>{item.title}</a> */}
                                                    <Link to={`/movie-details/${item.id}`} activeClassName="current">{item.title}</Link>
                                                </div>
                                            </Col>
                                        </div>
                                    )
                                })
                            }
                        </Row>
                        
                    </div>
                </div>
            </Content>
        )
    }
}

export default AllMovies