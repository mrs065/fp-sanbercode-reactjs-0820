import React from 'react';
import logo from './logo.svg';
import './App.css';
import 'antd/dist/antd.css';
import './style.css'
import Main from './Layout/Main'

function App() {
  return (
    <>
      <Main />
    </>
  );
}

export default App;
