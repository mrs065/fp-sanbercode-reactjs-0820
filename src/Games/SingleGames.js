import React, {useState, useEffect} from 'react'
import {useParams} from 'react-router-dom'
import axios from 'axios'
import { Breadcrumb, Layout, PageHeader } from 'antd';

const { Content } = Layout;

const SingleGames = () => {
    let {id} = useParams()
    const [games, setGames] = useState(null)

    useEffect(() => {
        if(games === null){
            axios.get(`https://backendexample.sanbersy.com/api/data-game/${id}`)
            .then(res => {
                setGames(res.data)
            })
        }
    }, [games, setGames])

    return (
        <Content style={{ margin: '0 16px' }}>
            <Breadcrumb style={{ margin: '16px 0' }}>
                <Breadcrumb.Item>Games</Breadcrumb.Item>
                <Breadcrumb.Item>Game Detail</Breadcrumb.Item>
            </Breadcrumb>
            <div className="site-layout-background" style={{ padding: 24, minHeight: 360 }}>
                
                <br />
                <div>
                    {games !== null &&
                        <>
                            <PageHeader className="site-page-header" title={games.name} />
                            <p>{games.genre}</p>
                            <p>{games.platform}</p>
                            <p><img width='200' src={games.image_url} /></p>
                        </>
                    }
                </div>
            </div>
        </Content>
    )
}

export default SingleGames