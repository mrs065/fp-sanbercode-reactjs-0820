import React, {useContext, useEffect, useState} from "react"
import axios from "axios"
import {GameContext} from "./GameContext"

const GameList = () => {
    const [game, setGame] = useContext(GameContext)
    const [input, setInput] = useState({titleGame: ''})

    useEffect(() => {
        if(game.lists === null){
            axios.get(`https://backendexample.sanbersy.com/api/data-game`)
            .then(res => {
                setGame({
                    ...game,
                    lists: res.data.map(el =>{
                        return{
                            id: el.id,
                            name: el.name,
                            genre: el.genre,
                            singlePlayer: el.singlePlayer,
                            multiplayer: el.multiplayer,
                            platform: el.platform,
                            release: el.release,
                            image_url: el.image_url
                        }
                    })
                })
            })
        }
    }, [setGame, game])

    const editGame = (event) => {
        let idGame = parseInt(event.target.value)
        setGame({...game, selectedId: idGame, statusForm: 'changeToEdit'})
    }

    const deleteGame = (event) => {
        let idGame = parseInt(event.target.value)
        let newGameList = game.lists.filter(el => el.id !== idGame)
        axios.delete(`https://backendexample.sanbersy.com/api/data-game/${idGame}`, {headers: {"Authorization" : `Bearer ${user.token}`}})
        .then(res=> {
            console.log(res)
        })
        setGame({...game, lists: [...newGameList]})
    }

    const filterGame = (event) => {
        let titleGame = event.target.value
        axios.get(`https://backendexample.sanbersy.com/api/data-game`)
        .then(res => {
            let result = game.lists.filter(el => el.title == titleGame)
            setGame(result)
        })
    }

    const handleChangeSearch = (event) => {
        var value = event.target.value
        setInput({titleMovie: value})
    }

    return(
        <>
            <input type='text' value={input.titleGame} onChange={handleChangeSearch} name='search'/>
            &nbsp;
            <button onClick={filterGame}>Search</button>
            <h1>Daftar Game</h1>
            <br />
            <table style={{borderCollapse: "collapse", margin: '0 auto'}}>
                <thead>
                    <tr style={{borderBottom: '1px solid #dddddd'}}>
                        <th>No</th>
                        <th>Name</th>
                        <th>Genre</th>
                        <th>Single Player</th>
                        <th>Multi Player</th>
                        <th>Platform</th>
                        <th>Release</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        game.lists !== null && game.lists.map((item, index) => {
                            return(
                                <tr key={index} style={{borderBottom: '1px solid #dddddd'}}>
                                    <td>{index+1}</td>
                                    <td>{item.name}</td>
                                    <td>{item.genre}</td>
                                    <td>{item.singlePlayer}</td>
                                    <td>{item.multiplayer}</td>
                                    <td>{item.platform}</td>
                                    <td>{item.release}</td>
                                    <td>
                                        <button onClick={editGame} value={item.id}>Edit</button>
                                        &nbsp;
                                        <button onClick={deleteGame} value={item.id}>Delete</button>
                                    </td>
                                </tr>
                            )
                        })
                    }
                </tbody>
            </table>
        </>
    )
}

export default GameList