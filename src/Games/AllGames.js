import React, {useState, useEffect, Component} from 'react'
import axios from 'axios'
import { Breadcrumb, Layout, PageHeader, Col, Row } from 'antd';
import { Link } from 'react-router-dom';

const { Content } = Layout;
const gutters = {};
const vgutters = {};

// function minuteToHours(num){
//     var hours = (num/60)
//     var rhours = Math.floor(hours)
//     var minutes = (hours - rhours)*60
//     var rminutes = Math.round(minutes)
//     return (rhours === 0 ? '' : rhours + ' Jam' + (rminutes===0 ? '' : ' ' + rminutes + ' Menit'))
// }

class AllGames extends Component {
    constructor(props){
        super(props)
        this.state = {
            dataGame: [],
            gutterKey: 16,
            vgutterKey: 16,
            colCountKey: 4
        }
    }

    componentDidMount(){
        axios.get(`https://backendexample.sanbersy.com/api/data-game`)
        .then(res => {
            let dataGame = res.data.map(el => { return {
                id: el.id,
                name: el.name,
                image_url: el.image_url
            }})
            this.setState({dataGame})
        })
    }

    render(){
        return(
            <Content style={{ margin: '0 16px' }}>
                <Breadcrumb style={{ margin: '16px 0' }}>
                    <Breadcrumb.Item>Games</Breadcrumb.Item>
                </Breadcrumb>
                <div className="site-layout-background" style={{ padding: 24, minHeight: 360 }}>
                    <PageHeader className="site-page-header" title="Data Game Terbaik" />
                    <br />
                    <div>
                        <Row gutter={[gutters[this.state.gutterKey], vgutters[this.state.vgutterKey]]}>
                            {
                                this.state.dataGame.map((item) => {
                                    return(
                                        <div>
                                            <Col span={6}>
                                                <div>
                                                    <img style={{width: "100%", height: "200px", objectFit: "cover", borderRadius: "4px"}} src={item.image_url} />
                                                    <br />
                                                    {/* <a>{item.name}</a> */}
                                                    <Link to={`/games-details/${item.id}`} activeClassName="current">{item.name}</Link>
                                                </div>
                                            </Col>
                                        </div>
                                    )
                                })
                            }
                        </Row>
                    </div>
                </div>
            </Content>
        )
    }
}

export default AllGames