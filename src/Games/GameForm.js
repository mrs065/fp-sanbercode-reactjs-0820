import React, {useContext, useState, useEffect} from "react"
import axios from "axios"
import {GameContext} from "./GameContext"

const GameForm = () => {
    const [game, setGame] = useContext(GameContext)
    const [input, setInput] = useState({
        name: '',
        genre: '',
        singlePlayer: '',
        multiplayer: '',
        platform: '',
        release: '',
        image_url: ''
    })

    useEffect(()=> {
        if(game.statusForm === 'changeToEdit'){
            let gameData = game.lists.find(x => x.id === game.selectedId)
            setInput({
                name: gameData.name,
                genre: gameData.genre,
                singlePlayer: gameData.singlePlayer,
                multiplayer: gameData.multiplayer,
                platform: gameData.platform,
                release: gameData.release,
                image_url: gameData.image_url
            })
            setGame({...game, statusForm: "edit"})
        }
    }, [game, setGame])

    const handleChange = (event) => {
        let typeOfInput = event.target.name

        switch(typeOfInput) {
            case "name":{
                setInput({...input, name: event.target.value});
                break
            }
            case "genre":{
                setInput({...input, genre: event.target.value});
                break
            }
            case "singlePlayer":{
                setInput({...input, singlePlayer: event.target.value});
                break
            }
            case "multiplayer":{
                setInput({...input, multiplayer: event.target.value});
                break
            }
            case "platform":{
                setInput({...input, platform: event.target.value});
                break
            }
            case "release":{
                setInput({...input, release: event.target.value});
                break
            }
            case "image_url":{
                setInput({...input, image_url: event.target.value});
                break
            }
            default: {break;}
        }
    }

    const handleSubmit = (event) => {
        event.preventDefault()

        if(game.statusForm === "create"){
            axios.post(`https://backendexample.sanbersy.com/api/data-game`, {name: input.name, genre: input.genre, singlePlayer: input.singlePlayer, multiplayer: input.multiplayer, platform: input.platform, release: input.release, image_url: input.image_url}, {headers: {"Authorization" : `Bearer ${user.token}`}})
            .then(res => {
                setMovie(
                    {statusForm: "create", selectedId: 0,
                        lists: [
                            ...movie.lists,
                            {
                                id: res.data.id,
                                name: input.name,
                                genre: input.genre,
                                singlePlayer: input.singlePlayer,
                                multiplayer: input.multiplayer,                                
                                platform: input.platform,
                                release: input.release,
                                image_url: input.image_url
                            }
                        ]
                    }
                )
            })
        } else if (game.statusForm === "edit"){
            axios.put(`https://backendexample.sanbersy.com/api/data-game/${game.selectedId}`, {name: input.name, genre: input.genre, singlePlayer: input.singlePlayer, multiplayer: input.multiplayer, platform: input.platform, release: input.release, image_url: input.image_url}, {headers: {"Authorization" : `Bearer ${user.token}`}})
            .then(() => {
                let gameData = game.lists.find(el => el.id === game.selectedId)
                gameData.name = input.name
                gameData.genre = input.genre
                gameData.singlePlayer = input.singlePlayer
                gameData.multiplayer = input.multiplayer
                gameData.platform = input.platform
                gameData.release = input.release
                gameData.image_url = input.image_url
                setGame({statusForm: "create", selectedId: 0, lists: [...game.lists]})
            })
        }
        setInput({
            name: '',
            genre: '',
            singlePlayer: '',
            multiplayer: '',
            platform: '',
            release: '',
            image_url: ''
        })
    }
    
    return(
        <div style={{width: "50%", margin: "0 auto", display: "block"}}>
            <div style={{border: "1px solid #aaa", padding: "20px"}}>
            <h1>Game Form</h1>
            <br />
            <form onSubmit={handleSubmit}>
                <label style={{float: "left"}}><b>Name</b></label>
                <input style={{float: "right"}} type='text' name='name' value={input.name} onChange={handleChange} />
                <br />
                <br />
                <label style={{float: "left"}}>Genre</label>
                <input style={{float: "right"}} type='text' name='genre' value={input.genre} onChange={handleChange} />
                <br />
                <br />
                <br />
                <label style={{float: "left"}}>Single Player</label>
                <input style={{float: "right"}} type='text' name='singlePlayer' value={input.singlePlayer} onChange={handleChange} />
                <br />
                <br />
                <label style={{float: "left"}}>Multi Player</label>
                <input style={{float: "right"}} type='text' name='multiplayer' value={input.multiplayer} onChange={handleChange} />
                <br />
                <br />
                <label style={{float: "left"}}>Platform</label>
                <input style={{float: "right"}} type='text' name='platform' value={input.platform} onChange={handleChange} />
                <br />
                <br />
                <label style={{float: "left"}}>Release</label>
                <input style={{float: "right"}} type='text' name='release' value={input.release} onChange={handleChange} />
                <br />
                <br />
                <label style={{float: "left"}}>Image URL</label>
                <textarea style={{float: "right"}} rows="3" name='image_url' value={input.image_url} onChange={handleChange}></textarea>
                <br />
                <br />
                <br />
                <br />
                <div style={{width: "100%", paddingBottom: "20px"}}>
                    <button style={{float: "center"}}>Submit</button>
                </div>
            </form>
            </div>
        </div>
    )
}

export default GameForm