import React from "react"
import {GameProvider} from "./GameContext"
import GameForm from "./GameForm"
import GameList from "./GameList"
import { getUser, removeUserSession } from '../Utils/Common';

const Game = (props) => {
    const user = getUser();
    const handleLogout = () => {    
        removeUserSession();
        props.history.push('/login');
    }
    
    return(
        <section>
            <input type="button" onClick={handleLogout} value="Logout" />
            <br />
            <br />
            <GameProvider>
                <GameList />
                <br />
                <GameForm />
            </GameProvider>
        </section>
    )
}

export default Game