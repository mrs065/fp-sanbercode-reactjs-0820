import React from 'react'
import { BrowserRouter as Router } from "react-router-dom";
import { Layout } from 'antd';
import Content from './Content'
import Header from './Header'
import Footer from './Footer'
import Sidebar from './Sidebar'
import AllMovies from '../Movie/AllMovies'
// import { Router } from 'react-router-dom';

// const { Header, Content, Footer, Sider } = Layout;

class Main extends React.Component {
    state = {
      collapsed: false,
    };
  
    onCollapse = collapsed => {
      console.log(collapsed);
      this.setState({ collapsed });
    };
  
    render() {
      return (
        <Router>
          <Layout style={{ minHeight: '100vh' }}>
            <Sidebar />
            <Layout className="site-layout">
              <Header />
              <Content />
              {/* <AllMovies /> */}
              <Footer />
            </Layout>
          </Layout>
        </Router>
        
      );
    }
  }

  export default Main