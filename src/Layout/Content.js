import React, { useContext } from 'react'
import {Switch, Route, Redirect} from 'react-router-dom'
import { Layout } from 'antd';
import AllMovies from '../Movie/AllMovies'
import AllGames from '../Games/AllGames'
import SingleMovies from '../Movie/SingleMovie'
import SingleGames from '../Games/SingleGames'
import Login from '../Auth/Login'
import Register from '../Auth/Register'
import { UserContext } from '../Auth/UserContext';
// import Movie from '../Movie/Movie'
// import Game from '../Games/Game'

const { Content } = Layout;

const Isi = () => {
    // const [user] = useContext(UserContext)

    // const PrivateRoute = ({user, ...props}) => {
    //     if(user){
    //         return <Route {...props} />
    //     } else {
    //         return <Redirect to='/login' />
    //     }
    // }

    // const LoginRoute = ({user, ...props}) =>
    // user ? <Redirect to='/' /> : <Route {...props} />

    return(
        <Content style={{ margin: '0 16px' }}>
            <Switch>
                {/* <Route exact path='/' component={Home} /> */}
                <Route exact path='/movie' component={AllMovies} />
                <Route exact path='/movie-details/:movieId' component={SingleMovies} />
                <Route exact path='/games' component={AllGames} />
                <Route exact path='/games-details/:gamesId' component={SingleGames} />
                <Route exact path='/login' component={Login} />
                <Route exact path='/register' component={Register} />
                {/* <LoginRoute exact path='/login' component={Login} />
                <LoginRoute exact path='/register' component={Register} /> */}
                {/* <PrivateRoute exact path='/movie-editor' component={Movie} />
                <PrivateRoute exact path='/game-editor' component={Game} /> */}
            </Switch>
        </Content>
    )
}

export default Isi