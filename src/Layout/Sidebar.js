import React, {useContext} from 'react'
import {Link} from 'react-router-dom'
import {UserContext} from '../Auth/UserContext'
import { Layout, Menu } from 'antd';
// import { UserOutlined, LaptopOutlined, NotificationOutlined } from '@ant-design/icons';
import {
    DesktopOutlined,
    PieChartOutlined,
    FileOutlined,
    TeamOutlined,
    UserOutlined,
  } from '@ant-design/icons';

const { Sider } = Layout;
const { SubMenu } = Menu;
// const [user, setUser] = useContext(UserContext)
// const handleLogout = () => {
//     setUser(null)
//     localStorage.removeItem('user')
// }


class Sidebar extends React.Component {
    state = {
        collapsed: false,
    };
    
    onCollapse = collapsed => {
        console.log(collapsed);
        this.setState({ collapsed });
    };

    // [user, setUser] = useContext(UserContext)
    // handleLogout = () => {
    //     setUser(null)
    //     localStorage.removeItem('user')
    // }


    render() {
        // const [user, setUser] = useContext(UserContext)
        // const handleLogout = () => {
        //     setUser(null)
        //     localStorage.removeItem('user')
        // }

        return (
            <Sider collapsible collapsed={this.state.collapsed} onCollapse={this.onCollapse}>
                <img id="logo" src={require("../img/logo.png")} width="200px" />
                <Menu theme="dark" defaultSelectedKeys={['1']} mode="inline">
                    <Menu.Item key="1" icon={<PieChartOutlined />}>
                        <Link to='/'>Home</Link>
                    </Menu.Item>
                    <Menu.Item key="2" icon={<DesktopOutlined />}>
                        <Link to='/movie'>Movie</Link>
                    </Menu.Item>
                    <Menu.Item key="3" icon={<DesktopOutlined />}>
                        <Link to='/games'>Games</Link>
                    </Menu.Item>
                    {/* {
                        user &&
                        <> */}
                            <Menu.Item key="4" icon={<DesktopOutlined />}>
                                <Link to='/movie-editor'>Movie Editor</Link>
                            </Menu.Item>
                            <Menu.Item key="5" icon={<DesktopOutlined />}>
                                <Link to='/game-editor'>Game Editor</Link>
                            </Menu.Item>
                        {/* </>
                    }
                    {
                        user === null &&
                        <> */}
                            <Menu.Item key="6" icon={<DesktopOutlined />}>
                                <Link to='/login'>Login</Link>
                            </Menu.Item>
                            <Menu.Item key="7" icon={<DesktopOutlined />}>
                                <Link to='/register'>Register</Link>
                            </Menu.Item>
                        {/* </>
                    }
                    {
                        user &&
                        <> */}
                            {/* <Menu.Item key="8" icon={<DesktopOutlined />}>
                                <Link onClick={handleLogout}>Logout</Link>
                            </Menu.Item> */}
                        {/* </>
                    } */}
                </Menu>
            </Sider>
        );
    }
}

export default Sidebar