import React from 'react'
import { Layout } from 'antd';

const { Footer } = Layout;
const Footers = () => {
    return (
        <Footer style={{ textAlign: 'center' }}>©2020 Created by Mega Resty Sudigdo</Footer>
    )
}

export default Footers